# -*- coding: utf-8 -*-
"""
Created on Sun Feb  2 12:17:11 2020

@author: zatim
"""

########
import os
import sys


######## SWITCH
LOG_PRINT_EN = 1

######## CONST

######## PRIVATE FUNC
def LogPrint(*args, **kwargs):
    def deco(func):
        if LOG_PRINT_EN:
            func("[LOG]", *args, **kwargs)
        return
    @deco
    def inner(*args, **kwargs):
        print(*args, **kwargs)
        return

    return inner


#######
class Tmp:
    ### private Vari
    __variPrivate = 1
    __attr = dict(param1=0,
                  param2=1,
              )

    ### public Vari
    variPublic = 2

    ### construct
    def __init__(self, tmp1, tmp2):
        self.__attr["param2"]=tmp2
        LogPrint("init %d,%d" %(self.__attr["param1"], self.__attr["param2"]))
        return


    ### private
    def __func(self):
        LogPrint("%s:%d" %(sys._getframe().f_code.co_name, self.__variPrivate ) )
        return

    ### public
    def Func(self):
        LogPrint("%s:%d, %d" %(sys._getframe().f_code.co_name, self.variPublic, 1 ) )
        self.__func()
        return
    
########
def main():

    LogPrint("main %d" %(1))
    
    ##create
    tmpcls = Tmp(**{"tmp1":1,"tmp2":3})
    tmpcls.Func()
    
    
    return


########
if __name__ == "__main__":
    print("START[%s]" %(os.path.basename(__file__)) )

    main()
    ###

    print("END")
